/*Хранение данных на клиенте

----------------------
1. IndexedDB Service - https://www.udacity.com/course/offline-web-applications--ud899

Можно использовать встроенную IndexedDB.
----------------------
2. Создайте простую игру-угадайку чисел. Используйте веб-хранилище для сохранения данных.
Компьютер генерирует случайное число с четырьмя различными цифрами
 * Крайняя левая цифра не должна быть 0 (ноль)
 * Для простоты назовём его abcd.
На каждом шаге игрок вводит четырехзначное число
 * Для простоты назовём его xyzw.
Когда игра заканчивается:
 * Спросите у игрока его ник. 
 * Сохраните ник внутри хранилища localstorage
Реализуйте список высоких результатов 
Sheep (Овца) означает, что цифра из xyzw содержится в abcd, но не в том же положении
 * Если существует две такие цифры, то Sheep(овцы) – 2
Ram (баран) означает, что цифра из xyzw содержится в abcd и она находится на той же позиции.
 * Если существует две таких цифры, то Ram(барана) – 2
Игра продолжается до тех пор, пока игрок не угадает число abcd
 * т. е. имеет 4 Ram(Баранов)
 Вводим имя и сохраняем его. Сохраняем результаты в кэш! И выводим таблицу выигрышей.
*/

const guessNumber = document.getElementById('guessNumber');
const input = document.querySelector('input');
const messageStep = document.querySelector('.messageStep');
const buttonSubmit = document.createElement('button');
const tableResults = document.createElement('div');

let randomArr;
let validNumber = true;
let counterSteps = 0;

const generateNumber = (() => {
    randomArr = [];
    randomArr[0] = Math.floor(Math.random() * Math.floor(10));

    while(randomArr[0] == '0') {
        randomArr[0] = Math.floor(Math.random() * Math.floor(10));
    }
    while(randomArr.length < 4) {
        let num = Math.floor(Math.random() * Math.floor(10));
    
        if(randomArr.indexOf(num) != -1) {
            num = Math.floor(Math.random() * Math.floor(10)); 
        } else{
            randomArr.push(num);
        }
    }
})();

console.log(randomArr);

const isValidNumberPerson = () => {
    if(input.value.length != 4 || isNaN(Number(input.value))) {
        messageStep.innerHTML = `Invalid entered number. Number must contain four different digits and can not start with 0. </br>`
        validNumber = false;
    } else {
        validNumber = true;
    }
};

const checkNumbers = () => {
    if(validNumber) {
        let arrPerson = input.value.split('');
        let countSheep = 0;
        let countRam = 0;

        for(let i = 0; i < randomArr.length; i++) {
            for(let j = 0; j < arrPerson.length; j++) {

                if((randomArr.indexOf(Number(arrPerson[j])) != -1) && (randomArr[i] != Number(arrPerson[j]))) {
                    countSheep++;
                }
    
                if(randomArr[i] == Number(arrPerson[j])) {
                    countRam++;
                }
                i++;
            }
        }
        messageStep.innerHTML += `You have: ${countRam} ram(s) and ${countSheep} sheep(s) </br>`;

        if(countSheep ==0 && countRam == 4) {
            const messageWin = document.createElement('p');
            const span = document.querySelector('span');
            buttonSubmit.classList.add('buttonSubmit');
            buttonSubmit.innerHTML = 'Submit username';
            span.innerHTML = 'Please enter your username: ';
            messageWin.innerHTML = 'Congratulations! You have guessed the number and win the game!'
            input.value = '';
            input.placeholder = 'Enter username';
            guessNumber.remove();
            messageStep.remove();
            span.before(messageWin);
            input.after(buttonSubmit);
        }
    }
    counterSteps++;
};

guessNumber.addEventListener('click', isValidNumberPerson);
guessNumber.addEventListener('click', checkNumbers);

let db;
const openRequest = indexedDB.open('myIndexedBD', 1);

openRequest.onupgradeneeded = (event) => {
    const myBD = event.target.result;
    if(!myBD.objectStoreNames.contains('people')) {
        myBD.createObjectStore('people', {autoIncrement: true})
    }
}

openRequest.onsuccess = (event) => {
    db = event.target.result;
}

const addPerson = () => {
    const transaction = db.transaction(['people'], 'readwrite');
    const store = transaction.objectStore('people');
    const userName = input.value;
    const steps = counterSteps;

    const person = {
        userName: userName,
        steps: steps
    };
    const request = store.add(person);

    request.onerror = function(event) {
        console.log('Error', event.target.error.userName);
    }

    request.onsuccess = function(event) {
        console.log('All ok');
    }
}

const viewPersons = () => {
    let transaction = db.transaction(['people'], 'readonly');
    let store = transaction.objectStore('people');
    let request = store.openCursor();

    request.onsuccess = (event) => {
        let cursor = event.target.result;

        if (cursor != null) {
            tableResults.innerHTML += `${cursor.value.userName}: ${cursor.value.steps} moves. </br>`;
            cursor.continue();
        } 
    }

    request.onerror = (event) => {
        alert('error in cursor request ' + event.target.errorCode);
    }
}

buttonSubmit.addEventListener('click', function() {
    addPerson();

    const game = document.querySelector('.game');
    const title = document.createElement('h1');
    title.innerHTML = 'Users results:';

    while(game.firstChild) {
        game.firstChild.remove();
    };
    game.append(title, tableResults);
    viewPersons();
})